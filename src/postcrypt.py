import time
import sys
import smime
import os
import libmilter


config_path = '/etc/postcrypt/'
log_file_path = '/var/log/postcrypt.log'


class PostCrypt(libmilter.ForkMixin, libmilter.MilterProtocol):
    def __init__(self, opts=0, protos=0):
        libmilter.MilterProtocol.__init__(self, opts, protos)
        libmilter.ForkMixin.__init__(self)

        self.recipient_key = None
        self.log_file = open(log_file_path, 'a')

    def set_recipient_key(self, recipient):
        try:
            with open(os.path.join(config_path, 'keys', recipient), 'rb') as key_file:
                self.recipient_key = key_file.read()
                return True
        except FileNotFoundError:
            return False
        except Exception as ex:
            self.log(str(ex))
            return False

    def log(self, message):
        timestamp = time.strftime('%H:%M:%S')
        self.log_file.write('[%s] %s\n' % (timestamp, message))

    @libmilter.noReply
    def connect(self, hostname, family, ip, port, cmd_dict):
        self.log('event=connect source_ip=%s:%d source_hostname=%s family=%s' % (
            ip,
            port,
            hostname,
            family))
        return libmilter.CONTINUE

    @libmilter.noReply
    def helo(self, helo_name):
        self.log('HELO: %s' % helo_name)
        return libmilter.CONTINUE

    @libmilter.noReply
    def mailFrom(self, frAddr, cmd_dict):
        self.log('MAIL: %s' % frAddr)
        return libmilter.CONTINUE

    @libmilter.noReply
    def rcpt(self, recipient, cmd_dict):
        self.log('RCPT: %s' % recipient)
        if self.set_recipient_key(recipient):
            return libmilter.CONTINUE
        else:
            return libmilter.ACCEPT

    @libmilter.noReply
    def header(self, key, val, cmd_dict):
        self.log('%s: %s' % (key, val))
        if str(key).lower() == 'content-type' and 'application/pkcs7-mime' in str(val).lower(): return libmilter.ACCEPT
        return libmilter.CONTINUE

    @libmilter.noReply
    def eoh(self, cmd_dict):
        self.log('EOH')
        return libmilter.CONTINUE

    def data(self, cmd_dict):
        self.log('DATA')
        return libmilter.CONTINUE

    @libmilter.noReply
    def body(self, chunk, cmd_dict):
        self.log('Body chunk: %d' % len(chunk))
        return libmilter.CONTINUE

    def eob(self, cmd_dict):
        self.log('EOB')
        # self.setReply('554' , '5.7.1' , 'Rejected because I said so')
        encrypted_body = smime.encrypt('\n'.join(self.body), self.recipient_key)
        self.replBody(encrypted_body)
        self.addHeader('Content-type', 'application/pkcs7-mime; name=smime.p7m; smime-type=enveloped-data')
        self.addHeader('Content-disposition', 'attachment; filename=smime.p7m')
        self.addHeader('Content-transfer-encoding', 'base64')
        return libmilter.CONTINUE

    def close(self):
        self.log('Close called. QID: %s' % self._qid)
        try:
            self.log_file.close()
        except:
            pass


def run_milter():
    import signal, traceback
    opts = libmilter.SMFIF_CHGBODY | libmilter.SMFIF_ADDHDRS
    milter = libmilter.ForkFactory('inet:127.0.0.1:5000', PostCrypt, opts)

    def signal_handler(num, frame):
        milter.close()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)

    try:
        milter.run()
    except Exception as e:
        print('EXCEPTION OCCURED: %s' % e)
        milter.close()
        traceback.print_tb(sys.exc_traceback)
        sys.exit(3)


if __name__ == '__main__':
    run_milter()
